import axios from 'axios';

const ms = {
  diseases: 'http://diseasesapi:8000',
  actions: 'http://actionsapi:8000',
  profiling: 'http://profilingapi:8000',
};

describe('diseases', () => {
  it('works', async () => {
    try {
      const {
        data: { diseases },
      } = await axios.get(ms.diseases + '/diseases');
      expect(diseases.length).toBe(0);
    } catch (e) {
      fail('failed connection');
    }
  });
});
