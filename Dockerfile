FROM node:14-alpine

ENV PORT=8000 \
    APP_PATH=/usr/src/app

WORKDIR ${APP_PATH}

COPY package.json yarn.lock ./

RUN yarn install

COPY . ./

EXPOSE ${PORT}

CMD yarn start 
